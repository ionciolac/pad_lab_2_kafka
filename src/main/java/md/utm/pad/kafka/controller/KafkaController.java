package md.utm.pad.kafka.controller;

import md.utm.pad.kafka.engine.Producer;
import md.utm.pad.kafka.model.Message;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

@RestController
@RequestMapping("/kafka")
public class KafkaController {

    private final Producer producer;

    @Autowired
    KafkaController(Producer producer) {
        this.producer = producer;
    }

    @PostMapping()
    public void sendMessageToKafkaTopic(@RequestBody Message message) {
        producer.sendMessage(message.getMessage());
    }

}
